#include "plugin.hpp"
#include <chrono>
#include <thread>

plugin::plugin() {
	// Find SAMP library in memory
	auto samp_addr = reinterpret_cast<std::uintptr_t>(GetModuleHandleA("samp.dll"));
	auto samp_size = reinterpret_cast<IMAGE_NT_HEADERS *>(samp_addr + reinterpret_cast<IMAGE_DOS_HEADER *>(samp_addr)->e_lfanew)->OptionalHeader.SizeOfImage;

	// Patch original frame limiter
	constexpr auto proc_fpslimit_bytes = "\x51\x56\x57\x8B\xF9\xE8";
	constexpr auto proc_fpslimit_mask  = "xxxxxx";
	proc_fpslimit_addr_				   = lemon::mem::find_pattern(samp_addr, samp_size, proc_fpslimit_bytes, proc_fpslimit_mask);
	lemon::mem::safe_set(proc_fpslimit_addr_, 0xC3, 1);

	// NOP min&max frame limiter values
	constexpr auto cmd_fpslimit_bytes = "\x83\xFE\x14\x72";
	constexpr auto cmd_fpslimit_mask  = "xxxx";
	cmd_fpslimit_addr_				  = lemon::mem::find_pattern(samp_addr, samp_size, cmd_fpslimit_bytes, cmd_fpslimit_mask);
	cmd_fpslimit_orig_.resize(10);
	lemon::mem::safe_copy(reinterpret_cast<std::uintptr_t>(cmd_fpslimit_orig_.data()), cmd_fpslimit_addr_, 10);
	lemon::mem::safe_set(cmd_fpslimit_addr_, 0x90, 10);

	constexpr auto read_fpslimit_bytes = "\x83\xF8\x14\x7C";
	constexpr auto read_fpslimit_mask  = "xxxx";
	read_fpslimit_addr_				   = lemon::mem::find_pattern(samp_addr, samp_size, read_fpslimit_bytes, read_fpslimit_mask);
	if (read_fpslimit_addr_) {
		read_fpslimit_orig_.resize(10);
		lemon::mem::safe_copy(reinterpret_cast<std::uintptr_t>(read_fpslimit_orig_.data()), read_fpslimit_addr_, 10);
		lemon::mem::safe_set(read_fpslimit_addr_, 0x90, 10);
	} else {
		constexpr auto vmp_read_fpslimit_bytes = "\x0F\x8C\x00\x00\x00\x00\x60\x66\xC7\x44\x24\x04\x2F\x90";
		constexpr auto vmp_read_fpslimit_mask  = "xx????xxxxxxxx";
		read_fpslimit_addr_					   = lemon::mem::find_pattern(samp_addr, samp_size, vmp_read_fpslimit_bytes, vmp_read_fpslimit_mask);
		read_fpslimit_orig_.resize(12);
		// nop JL
		lemon::mem::safe_copy(reinterpret_cast<std::uintptr_t>(read_fpslimit_orig_.data()), read_fpslimit_addr_, 6);
		lemon::mem::safe_set(read_fpslimit_addr_, 0x90, 6);
		// nop jG
		lemon::mem::safe_copy(reinterpret_cast<std::uintptr_t>(read_fpslimit_orig_.data()) + 6, read_fpslimit_addr_ + 0x1C, 6);
		lemon::mem::safe_set(read_fpslimit_addr_ + 0x1C, 0x90, 6);
	}

	// Patch GTA SA
	lemon::mem::safe_set(0x53E94C, 0, 1);												  // Remove 14ms delay
	lemon::mem::safe_copy(reinterpret_cast<void *>(0x745240), "\xB0\x00\x90\x90\x90", 5); // Unlock 60 FPS with game lock

	// Setup new frame limiter
	idle_hook_.on_before += std::make_tuple(this, &plugin::on_idle);
	idle_hook_.install();
}

plugin::~plugin() {
	// Remove new frame limiter
	idle_hook_.remove();

	// Unpatch GTA SA
	lemon::mem::safe_copy(reinterpret_cast<void *>(0x745240), "\xA0\x94\x67\xB0\x00", 5);
	lemon::mem::safe_set(0x53E94C, 14, 1);

	// Restore original min&max frame limiter values
	if (read_fpslimit_orig_.size() == 10) {
		lemon::mem::safe_copy(read_fpslimit_addr_, reinterpret_cast<std::uintptr_t>(read_fpslimit_orig_.data()), 10);
	} else {
		lemon::mem::safe_copy(read_fpslimit_addr_, reinterpret_cast<std::uintptr_t>(read_fpslimit_orig_.data()), 6);
		lemon::mem::safe_copy(read_fpslimit_addr_ + 0x1C, reinterpret_cast<std::uintptr_t>(read_fpslimit_orig_.data()) + 6, 6);
	}
	lemon::mem::safe_copy(cmd_fpslimit_addr_, reinterpret_cast<std::uintptr_t>(cmd_fpslimit_orig_.data()), 10);
}

void plugin::on_idle() {
	static auto &game = **reinterpret_cast<std::uintptr_t **>(cmd_fpslimit_addr_ + 0xA + 2);
	if (!game) return;

	auto		 is_fpslimit	   = *reinterpret_cast<std::uint8_t *>(0xBA6794) == 1;
	static auto &cped			   = *reinterpret_cast<std::uintptr_t *>(0xB6F5F0);
	auto		 is_touching_water = cped != 0 && *reinterpret_cast<std::uint8_t *>(cped + 0x43) & 8;
	if (!is_fpslimit && !is_touching_water) return;

	using namespace std::chrono_literals;
	static constexpr auto ms_to_ns = static_cast<float>(std::chrono::duration_cast<std::chrono::nanoseconds>(1ms).count());
	static auto			  previous = std::chrono::steady_clock::now();

	static auto cgame_fpslimit_off = *reinterpret_cast<std::uint8_t *>(proc_fpslimit_addr_ + 0xE7 + 2);
	auto		fpslimit		   = *reinterpret_cast<int *>(game + cgame_fpslimit_off);
	if (is_touching_water && fpslimit > 45) fpslimit = 45;

	auto fps_duration  = std::chrono::nanoseconds(static_cast<std::uint64_t>(1000.0f / static_cast<float>(fpslimit) * ms_to_ns));
	auto loop_duration = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::steady_clock::now() - previous);
	if (loop_duration <= fps_duration) std::this_thread::sleep_for(fps_duration - loop_duration);
	previous = std::chrono::steady_clock::now();
}