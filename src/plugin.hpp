#ifndef PLUGIN_HPP
#define PLUGIN_HPP

#include <vector>
#include <lemon/hook.hpp>

class plugin {
	std::uintptr_t proc_fpslimit_addr_ = 0;

	std::uintptr_t			  cmd_fpslimit_addr_ = 0;
	std::vector<std::uint8_t> cmd_fpslimit_orig_;

	std::uintptr_t			  read_fpslimit_addr_ = 0;
	std::vector<std::uint8_t> read_fpslimit_orig_;

	lemon::hook<> idle_hook_{ 0x53ECBD };

public:
	plugin();
	~plugin();

private:
	void on_idle();
};

#endif // PLUGIN_HPP
