#include <memory>
#include <lemon/hook.hpp>
#include "plugin.hpp"

std::unique_ptr<plugin> plug;

void gameloop() {
	static bool init = false;
	if (init) return;
	plug = std::make_unique<plugin>();
	init = true;
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD dwReasonForCall, LPVOID) {
	static lemon::hook<> gameloop_hook(0x748DA3);
	if (dwReasonForCall == DLL_PROCESS_ATTACH) {
		gameloop_hook.on_before += &gameloop;
		gameloop_hook.install();
	}
	return TRUE;
}
